package sheridan;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import sheridan.Palindrome;
public class PalindromeTest {

	@Test
	public void testIsPalindrome( ) {
		boolean test = Palindrome.isPalindrome("racecar");
		assertTrue(test);
	}

	@Test
	public void testIsPalindromeNegative( ) {
		boolean test = Palindrome.isPalindrome("asdfg");
		assertFalse(test);
	}
	@Test
	public void testIsPalindromeBoundaryIn( ) {
		boolean test = Palindrome.isPalindrome("rac e car");
		assertTrue(test);
	}
	@Test
	public void testIsPalindromeBoundaryOut( ) {
		boolean test = Palindrome.isPalindrome("Racecar");
		assertFalse(test);
	}	
	
}
